//Modules
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

//Components
import {AppComponent}  from './app.component';
import {CarouselComponent} from './carousel/carousel.component';
//Services
import {FeedsService} from './services/feeds.service';


@NgModule({
    imports: [NgbModule.forRoot(), BrowserModule, HttpModule, JsonpModule],
    declarations: [AppComponent, CarouselComponent],
    bootstrap: [AppComponent],
    providers: [FeedsService]
})
export class AppModule {
}
