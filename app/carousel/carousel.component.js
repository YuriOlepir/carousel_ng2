"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var feeds_service_1 = require('../services/feeds.service');
var CarouselComponent = (function () {
    function CarouselComponent(feedsService) {
        this.feedsService = feedsService;
        this.carouselArray = [];
        this.carouselNews = [];
        this.carouselFeed = [];
        this.urlParamsId = [32, 64, 66];
    }
    CarouselComponent.prototype.getFeeds = function () {
        var _this = this;
        for (var _i = 0, _a = this.urlParamsId; _i < _a.length; _i++) {
            var params = _a[_i];
            this.feedsService.getFeeds(params)
                .subscribe(function (response) {
                _this.carouselArray.push(response.news);
                _this.carouselFeed.push(response.feed.name);
                if (_this.carouselArray.length === 3) {
                    _this.makeCarouselDesktop();
                }
                return _this.carouselFeed;
            }, function (err) {
                console.log(err);
            });
        }
    };
    CarouselComponent.prototype.makeCarouselDesktop = function () {
        this.carouselArray.sort(function (a, b) {
            return a - b;
        });
        for (var i = 0; i < this.carouselArray.slice(-1)[0].length; i++) {
            this.carouselNews.push([
                this.carouselArray[0][i],
                this.carouselArray[1][i],
                this.carouselArray[2][i]
            ]);
        }
    };
    CarouselComponent.prototype.ngOnInit = function () {
        this.getFeeds();
    };
    CarouselComponent = __decorate([
        core_1.Component({
            selector: 'carousel-comp',
            templateUrl: "app/carousel/carousel.html"
        }), 
        __metadata('design:paramtypes', [feeds_service_1.FeedsService])
    ], CarouselComponent);
    return CarouselComponent;
}());
exports.CarouselComponent = CarouselComponent;
//# sourceMappingURL=carousel.component.js.map