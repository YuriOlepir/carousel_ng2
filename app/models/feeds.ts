export class Feeds {
    feed: {
        name: string;
    };
    paging: {
        next: string;
    };
    news: {
        id: number;
        title: string;
        link: string;
        description: string;
        image: string;
        publication_time: Date;
        source: {
            icon: string;
            name: string;
        };
    };
}




