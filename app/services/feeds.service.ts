import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';

@Injectable()
export class FeedsService {
    private feedsUrl = 'http://newsagg.innocode.no/api/feeds/';

    constructor(private http: Http) {
    }


    getFeeds(params: number): Observable<any> {

        return this.http.get(this.feedsUrl+ params)

            .map((res: Response) => res.json())

            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }


}