import {Component, OnInit} from '@angular/core';

import {FeedsService} from '../services/feeds.service';


@Component({
    selector: 'carousel-comp',
    templateUrl: `app/carousel/carousel.html`
})

export class CarouselComponent implements OnInit {

    carouselResponseNews: any[] = [];
    carouselNews: any[] = [];
    carouselFeed: any[] = [];
    carouselLongestArray: any[] = [];

    urlParamsId = [32, 64, 66];

    constructor(private feedsService: FeedsService) {
    }

    getFeeds() {
        for (let params of this.urlParamsId) {
            this.feedsService.getFeeds(params)
                .subscribe(
                    response => {
                        this.carouselResponseNews.push(response.news);
                        this.carouselFeed.push(response.feed.name);
                        if (this.carouselResponseNews.length === this.urlParamsId.length) {
                            this.makeCarouselDesktop();
                        }
                        return this.carouselFeed;
                    },
                    err => {
                        console.log(err);
                    });
        }
    }

    makeCarouselDesktop() {
        this.carouselLongestArray = this.carouselResponseNews;
        this.carouselLongestArray.sort(function (a: any, b: any) {
            return a - b;
        });

        for (let i = 0; i < this.carouselLongestArray.slice(-1)[0].length; i++) {
            this.carouselNews.push([
                this.carouselResponseNews[0][i],
                this.carouselResponseNews[1][i],
                this.carouselResponseNews[2][i]
            ]);
        }
    }

    ngOnInit() {
        this.getFeeds();
    }

}